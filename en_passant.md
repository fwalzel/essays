## En passant 

*Graffito in Darmstadt*

Schnell arbeiten, schnell sterben.

--

*Im Radio gehört I*

Weihnachten ist das Fest der Kooperation, Weihnachten ist auch das Fest der Dopaminausschüttung.

--

*Im Radio gehört II*

Heimat to go.

--

*Pam hat es gesagt I*

Aller Anschein ist gleich gültig.

--

*Pam hat es gesagt II*

Wir *sind* ja der Tod.

--

