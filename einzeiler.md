## Einzeiler


1.

Alles geht vorüber, auch der Tod.

--

2.

Es ist dumm, andere nicht zu ehren, weil man sich selbst dadurch Ehre versagt.

--

3.

Das Unverhandelbare gebiert mehr Gewalt als das Verhandelbare.

--

4.

Ein gutes Motto: Wenn ihr wisst, was ihr wollt, kommt auf mich zu.

--

5.

Die erste und letzte der persönlichen Frage: Woran soll man sich verschwenden?

--

6.

Sterblichkeit wird überschätzt. Wer war ist in die Unendlichkeit eingraviert.

--

7.

Das Erste, wovon der vernünftige Mensch Abstand nimmt, ist die Utopie.

--

8.

Kein Wunsch ist wünschenswert, Frieden ausgenommen.

--

9.

Jeder menschlich gedachte Gott legt den Grundstein zur Unmenschlichkeit.

--

10.

Spionza hat recht, aus Gott geht nichts hervor.

--

11.

Gott ist ein herrliches Wort, es beendet so vieles.

--

12.

Alle sittlichen Deklinationen die vom angeblichen Willen Gottes abgeleitet werden, sind Blasphemie.

--

13.

Fremdheit ist ein anderer Bergriff für zu wenig Zeit ins Kennenlernen investiert zu haben. Das tragische daran ist, dass uns schon wegen unserer Lebenskürze das meiste Fremd bleiben muss. 
Es kommt darauf an wie man sich zu dieser Fremdheit stellt.

--

14.

Was sagt es über Gott, dass dieser sich so unzulängliche Wesen wie die Menschen zu den Kritikern seines Kosmos berufen hat?

--

15.

Substanzielle Verbesserungen, sind Verbesserungen der Dialektik.

--

16.

Der Einzigste, das Superste, das Göttlichste. Wir steigern auch das Absolute.

--

17.

Euphemismen sind schnell durchschaubar. Eine Lüge in nur einem Wort kann nicht gelingen.

--